#include <winsock2.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#define SERVER_PORT htons(4553)
using namespace std;


int main()
{
	int serverS;
	bool loop = false;
	struct sockaddr_in svrAdd, clntAdd;
	int len;
	int conn;
	WSADATA info;
	int err;
	int flag = 0;
	/////////////////////////////////////////////////
	cout << "Startuping this socket..." << endl;
	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err != 0)
	{
		cout << "err -stage[1]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "Creating this socket..." << endl;
	serverS = socket(AF_INET, SOCK_STREAM, 0);
	if (serverS == INVALID_SOCKET)
	{
		cout << "err-stage[2]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////
	memset((char*)&svrAdd, 0, sizeof(svrAdd));
	/////////////////////////////////////////////////
	cout << "Creating struct of the server" << endl;
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = SERVER_PORT;
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	//bind socket
	/////////////////////////////////////////////////
	cout << "binding the server..." << endl;
	if (bind(serverS, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr)) < 0)
	{
		cerr << "err-stage[3]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;

	/////////////////////////////////////////////////
	cout << "Listening for a connections..." << endl;
	listen(serverS, 4);

	while (flag == 0)
	{
		len = sizeof(clntAdd);
		conn = accept(serverS, (struct sockaddr *)&clntAdd, &len);
		if (conn < 0)
		{
			cerr << "Cannot accept connection" << endl;
			break;
		}
		else
		{
			//sending and disconnecting
			/////////////////////////////////////////////////
			cout << "user connected!! sending data..." << endl;
			send(serverS, "ACCEPTED", sizeof("ACCEPTED"), 0);
			cout << "The data have been sent!" << endl;
			cout << "closing the socket..." << endl;
			conn = closesocket(serverS);
			if (conn == SOCKET_ERROR)
			{
				cout << "err-stage[4]" << endl;
				break;
			}
			cout << "succeed to close socket! " << endl;
			/////////////////////////////////////////////////
			//creating a new socket
			/////////////////////////////////////////////////
			cout << "Creating this socket..." << endl;
			int serverS = socket(AF_INET, SOCK_STREAM, 0);
			if (serverS == INVALID_SOCKET)
			{
				cout << "err-stage[2]" << endl;
				break;
			}
			cout << "succeed..." << endl << endl;
			/////////////////////////////////////////////////
			//binding and listening
			/////////////////////////////////////////////////
			cout << "binding the server..." << endl;
			if (bind(serverS, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr)) < 0)
			{
				cerr << "err-stage[3]" << endl;
				break;
			}
			cout << "succeed..." << endl << endl;
			cout << "Listening for a connections..." << endl;
			listen(serverS, 4);
			/////////////////////////////////////////////////

		}
		break;
	}
	conn = closesocket(serverS);
	WSACleanup();
	return 0;
}
