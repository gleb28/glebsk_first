
#include <winsock2.h>
#include <windows.h>
#include <iostream>
#include <string>


using namespace std;
#define SERVER_PORT htons(4553)
int main()
{
	int len;
	int conn;
	struct sockaddr_in  clntAdd;
	char recStr[1000];
	WSADATA info;
	int err;
	/////////////////////////////////////////////////
	cout << "Startuping this socket..." << endl;
	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err != 0)
	{
		cout << "err -stage[1]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "Creating this socket..." << endl;
	int serverS = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);
	if (serverS == INVALID_SOCKET)
	{
		cout << "err-stage[2]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "Creating struct of the server" << endl;
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = SERVER_PORT;
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	
	/////////////////////////////////////////////////
	cout << "binding the server..." << endl;
	if (bind(serverS, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr)) < 0)
	{
		cerr << "err-stage[3]" << endl;
		return 0;
	}
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////
	cout << "Listening for a connections..." << endl;
	listen(serverS, 3);
	len = sizeof(clntAdd);
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "waiting for user to connect..." << endl;
	conn = accept(serverS, (struct sockaddr *)&clntAdd,&len);
	cout << "succeed..." << endl << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "user connected!! sending data..." << endl;
	send(serverS, "ACCEPTED", sizeof("ACCEPTED"), 0);
	cout << "The data have been sent!" << endl;
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	cout << "closing the socket..." << endl;
	conn = closesocket(serverS);
	if (conn == SOCKET_ERROR)
	{
		cout << "err-stage[4]" << endl;
	}
	WSACleanup();
	cout << "succeed to close socket! " << endl;
	/////////////////////////////////////////////////

	system("PAUSE");
	return 0;
}

